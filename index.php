<?php

require_once __DIR__ . '/vendor/autoload.php';

use app\Bridge\Application;

new Application();
