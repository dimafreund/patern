<?php
namespace app\Fabrica;

interface ProductInterface {

	public function getName();

	public function setName($name);

	public function getPrice();
}
