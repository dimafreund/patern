<?php
namespace app\Fabrica;

class Orange implements ProductInterface {

	private $name;

	public function setName($name)
	{
		$this->name = $name;
	}

	public function getName()
	{
		return $this->name;
	}

	public function getPrice()
	{
		return 22;
	}

}
