<?php
namespace app\Fabrica;

class Application {

	public $object;

	public function __construct($product)
	{
		switch ($product) {
			case 'Apple' :
				$this->object = new AppleFabric();
				break;
			case 'Orrange' :
				$this->object = new OrangeFabric();
				break;
			default:
				throw new \Exception('wrong name Object');
		}

		$this->object->DisplayInfo();

	}

}
