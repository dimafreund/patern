<?php
namespace app\Fabrica;

use app\Fabrica\Orange;
use app\Fabrica\Fabrica;

class OrangeFabric extends Fabrica {

	public function CreateProduct()
	{
		return new Orange();
	}

}
