<?php
namespace app\Fabrica;

use app\Fabrica\Apple;
use app\Fabrica\Fabrica;

class AppleFabric extends Fabrica {

	public function CreateProduct()
	{
		return new Apple();
	}
}
