<?php
namespace app\Fabrica;

abstract class Fabrica {

	protected $product;

	public function __construct()
	{
		$this->product = $this->createProduct();
	}

	abstract public function createProduct();

	public function DisplayInfo()
	{
		echo $this->product->getName() . ' have price - ' . $this->product->getPrice();
	}
}
