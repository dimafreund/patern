<?php
namespace app\Fabrica;

class Apple implements ProductInterface {

	private $name;

	public function setName($name)
	{
		$this->name = $name;
	}

	public function getName()
	{
		return $this->name . ' another realisation';
	}

	public function getPrice()
	{
		return 35;
	}

}
