<?php

namespace app\Bridge;

class BlogPage extends Page {

	protected $title;
	protected $date;
	protected $content;

	public function __construct(Theme $theme, $title, $date, $content)
	{
		parent::__construct($theme);
		$this->title = $title;
		$this->date = $date;
		$this->content = $content;
	}

	public function view()
	{
		$html = '';
		$html .= $this->theme->renderHeader($this->title);
		$html .= $this->theme->renderDate($this->date);
		$html .= $this->theme->renderContent($this->content);

		return $html;
	}

}
