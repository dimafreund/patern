<?php
namespace app\Bridge;

abstract class Page {

	protected $theme;

	public function __construct($theme)
		{
			$this->theme = $theme;
		}

	public function changeTheme($theme)
		{
			$this->theme = $theme;
		}

	abstract public function view();

}
