<?php

namespace app\Bridge;

class ProductPage extends Page {

	protected $title;
	protected $price;
	protected $weight;
	protected $content;

	public function __construct(Theme $theme, $title, $price, $weight, $description)
	{
		parent::__construct($theme);
		$this->title = $title;
		$this->date = $price;
		$this->weight = $weight;
		$this->description = $description;
	}

	public function view()
	{
		$this->render->renderHeader($this->title);
		$this->render->renderPrice($this->price);
		$this->render->renderWeight($this->weight);
		$this->render->renderDescription($this->description);
	}

}
