<?php
namespace app\Bridge;

class Application {

	public function __construct()
	{
		$theme = new DarkTheme();

		$page =  new BlogPage($theme, 'How create blog page?', 'today', 'lorem ipsum');

		echo $page->view();

		$page->changeTheme(new BlueTheme());

		echo $page->view();
	}

}
