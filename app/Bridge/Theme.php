<?php

namespace app\Bridge;

interface Theme {

	public function renderHeader(string $title);

	public function renderPrice(int $price);

	public function renderContent(string $content);

	public function renderDescription(string $content);

	public function renderDate(string $date);

	public function renderWeight(int $weight);
}
