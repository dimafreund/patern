<?php

namespace app\Bridge;

use app\Bridge\Theme;

class DarkTheme implements Theme {

	private $style = 'background: black; color: white;';

	public function renderHeader(string $title)
	{
		return $this->renderTag($title, 'h1', $this->style);
	}

	public function renderPrice(int $price)
	{
		return $this->renderTag($price, 'span', $this->style);
	}

	public function renderContent(string $content)
	{
		return $this->renderTag($content, 'div', $this->style);
	}

	public function renderDescription(string $content)
	{
		return $this->renderTag($content, 'p', $this->style);
	}

	public function renderDate(string $date)
	{
		return $this->renderTag($date, 'span', $this->style);
	}

	public function renderWeight(int $weight)
	{
		return $this->renderTag($weight, 'div', $this->style);
	}

	protected function renderTag($content, $tag, $style)
	{
		return "<$tag style=\"$style\">$content</$tag>";
	}

}
